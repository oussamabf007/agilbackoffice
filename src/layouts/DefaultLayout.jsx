import React from "react";
import {
  ListItemIcon,
  ListItemText,
  ListItem,
  List,
  IconButton,
  Hidden,
  Drawer,
  Divider,
  CssBaseline,
  AppBar,
  Toolbar,
  Typography,
  Menu,
  MenuItem,
} from "@material-ui/core";

import MenuIcon from "@material-ui/icons/Menu";
import HomeIcon from "@material-ui/icons/Home";
import AccessAlarmIcon from "@material-ui/icons/AccessAlarm";
import CategoryIcon from "@material-ui/icons/Category";
import GroupIcon from "@material-ui/icons/Group";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import AccountCircle from "@material-ui/icons/AccountCircle";
import SettingsIcon from "@material-ui/icons/Settings";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import PersonIcon from "@material-ui/icons/Person";
import PersonOutlinedIcon from "@material-ui/icons/PersonOutlined";
import NotesOutlinedIcon from "@material-ui/icons/NotesOutlined";
import DateRangeOutlinedIcon from "@material-ui/icons/DateRangeOutlined";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import PlaceOutlinedIcon from "@material-ui/icons/PlaceOutlined";

import { makeStyles, createMuiTheme, useTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

import { Link } from "react-router-dom";

import { useDispatch } from "react-redux";
import { loggOut } from "../providers/actions";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("md")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    marginTop: "64px",
    padding: theme.spacing(3),
  },
  link: {
    textDecoration: "none",
    color: "inherit",
  },
}));

const DefaultLayout = (props) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { window, children } = props;
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenuAccount = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuAccountClose = () => {
    setAnchorEl(null);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar}>
        <Typography
          variant="h6"
          noWrap
          style={{
            color: "#FFD733",
            width: "100%",
            textAlign: "center",
            height: "64px",
            display: "flex",
            fontSize: "32px",
          }}
        >
          <Link to="/" className={classes.link} style={{ margin: "auto" }}>
            <h6 style={{ margin: 0 }}>SNDP</h6>
          </Link>
        </Typography>
      </div>
      <Divider />
      <List>
        <Link to="/" className={classes.link}>
          <ListItem button key={" Evénements du jour"}>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary={" Evénements du jour"} />
          </ListItem>
        </Link>
      </List>
      <List>
        <Link to="/imprime" className={classes.link}>
          <ListItem button key={"Imprimés"}>
            <ListItemIcon>
              <NotesOutlinedIcon />
            </ListItemIcon>
            <ListItemText primary={"Imprimés"} />
          </ListItem>
        </Link>
      </List>
      {/*  
      <List>
        <Link to="/location" className={classes.link}>
          <ListItem button key={"Location"}>
            <ListItemIcon>
              <LocationOnIcon />
            </ListItemIcon>
            <ListItemText primary={"Location"} />
          </ListItem>
        </Link>
      </List>
      */}
      <List>
        <Link to="/evenement" className={classes.link}>
          <ListItem button key={"Evenements"}>
            <ListItemIcon>
              <DateRangeOutlinedIcon />
            </ListItemIcon>
            <ListItemText primary={"Evénements"} />
          </ListItem>
        </Link>
      </List>

      {/*   <List>
        <Link to="/frequences" className={classes.link}>
          <ListItem button key={"Frequence"}>
            <ListItemIcon>
              <AccessAlarmIcon />
            </ListItemIcon>
            <ListItemText primary={"Frequence"} />
          </ListItem>
        </Link>
      </List> */}
      <List>
        <Link to="/category" className={classes.link}>
          <ListItem button key={"Installation"}>
            <ListItemIcon>
              <CategoryIcon />
            </ListItemIcon>
            <ListItemText primary={"Installations"} />
          </ListItem>
        </Link>
      </List>
      <List>
        <Link to="/controle" className={classes.link}>
          <ListItem button key={"Controles"}>
            <ListItemIcon>
              <GroupIcon />
            </ListItemIcon>
            <ListItemText primary={"Contrôles"} />
          </ListItem>
        </Link>
      </List>
      <List>
        <Link to="/responsible" className={classes.link}>
          <ListItem button key={"Responsables"}>
            <ListItemIcon>
              <SupervisorAccountIcon />
            </ListItemIcon>
            <ListItemText primary={"Responsables"} />
          </ListItem>
        </Link>
      </List>
      <List>
        <Link to="/concerne" className={classes.link}>
          <ListItem button key={"Concernés"}>
            <ListItemIcon>
              <PersonOutlinedIcon />
            </ListItemIcon>
            <ListItemText primary={"Concernés"} />
          </ListItem>
        </Link>
      </List>
      <List>
        <Link to="/emplacement" className={classes.link}>
          <ListItem button key={"Emplacements"}>
            <ListItemIcon>
              <PlaceOutlinedIcon />
            </ListItemIcon>
            <ListItemText primary={"Emplacements"} />
          </ListItem>
        </Link>
      </List>

      {/*   <List>
        <Link to="/settings" className={classes.link}>
          <ListItem button key={"Settings"}>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary={"Settings"} />
          </ListItem>
        </Link>
      </List> */}
      <Divider />
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap style={{ flexGrow: 1 }}></Typography>
          <div>
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenuAccount}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={open}
              onClose={handleMenuAccountClose}
            >
              <MenuItem
                onClick={() => {
                  dispatch(loggOut());
                  handleMenuAccountClose();
                }}
                style={{ width: "150px" }}
              >
                Logout
                <ExitToAppIcon
                  style={{ position: "absolute", right: "10px" }}
                />
              </MenuItem>
              {/* <Link to="/profile" className={classes.link}>
                <MenuItem
                  onClick={handleMenuAccountClose}
                  style={{ width: "150px" }}
                >
                  Profile
                  <PersonIcon style={{ position: "absolute", right: "10px" }} />
                </MenuItem>
              </Link> */}
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden mdUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>{children}</main>
    </div>
  );
};

export default DefaultLayout;
