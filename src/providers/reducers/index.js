import isLoggedReducer from "./isLoggedIn";

import { combineReducers } from "redux";

const allReducers = combineReducers({
  isLogged: isLoggedReducer,
});

export default allReducers;
