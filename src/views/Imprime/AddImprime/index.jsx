import { useState } from "react";
import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import { useHistory } from "react-router-dom";
import { Button, Grid, Paper, TextField } from "@material-ui/core";

const AddImprime = () => {
  const [LoadingOpen, setLoadingOpen] = useState(false);

  const history = useHistory();
  const onSubmitHandler = async (e) => {
    e.preventDefault();
    const name = e.target.name.value;
    const description = e.target.description.value;
    if (!name && !description) {
      alert("il faut remplir le champ");
      return false;
    }
    try {
      setLoadingOpen(true);
      const res = await API(
        "POST",
        `${process.env.REACT_APP_API_BASE_URL}/imprimes`,
        { name, description }
      );
      if (res.code === 200) {
        alert("Imprime ajouté avec succés");
        history.push("/imprime");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
    } catch (error) {
      setLoadingOpen(false);
      console.log(error);
    }
  };
  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Ajouter Imprime </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Nom"
                  name="name"
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Désignation"
                  name="description"
                  fullWidth
                />
              </div>
              <div align="center">
                <Button variant="contained" color="primary" type="submit">
                  Ajouter
                </Button>{" "}
                <Button
                  variant="contained"
                  color="warning"
                  onClick={() => {
                    history.goBack();
                  }}
                >
                  Annuler
                </Button>
              </div>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddImprime;
